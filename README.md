# Test Front-end Zydax


######  Este projeto pode ser de sua preferencia, fique a vontade em usar vanilajs, Jquery, Angular, React ou Vuejs e etc.

###  O uso de um framework css é obrigatio:
bootstrap : [Links](https://getbootstrap.com/)
materializecss : [Links](https://materializecss.com/)

#### A porto paralelo api é uma api que retorna uma lista de lojas no Porto em Portugal:
os links de acesso sao:

stores -> [Links](https://pportoparalelo.herokuapp.com/api/stores/)

Areas -> [Links](https://pportoparalelo.herokuapp.com/api/areas)

Categories ->[Links](https://pportoparalelo.herokuapp.com/api/categories)

Para acessar uma pagina de detalhe voce pode usar:

detalhe da loja com id : [Links](https://pportoparalelo.herokuapp.com/api/stores/1)

ou com slug

Detalhe da loja com slug : [Links](https://pportoparalelo.herokuapp.com/api/stores/a-costa-real)

Para este exercicio: crie um projeto no seu github e faca o seguinte exercicio:
 
Crie uma lista de lojas com nome, foto(thumb ou medium), nome da categoria, nome da area e uma parte do texto da descricao da loja.
Pagina de detalhe da pagina exibir, nome, foto(large), nome da categoria, nome da area, a descricao completa,
mostrar os campos extras da API tambem sera bastante interessane, porem nāo é necessario. entao sinta-se a vontade para exibir mais campos se quiser.
ex: a galeria de imagem.


Extras:
- Mostrar os horarios dos dias da semana, e dizer se a loja esta aberta ou fechada.

- Barra de busca por nome, da loja,
ex: https://pportoparalelo.herokuapp.com/api/stores?search=costa
O retorno sera da loja "A. Costa Real"

- Usar um framework como react, vue, ou angular ou apenas vanila js com webpack -> preferencia vuejs
- Trocar o idioma de ingles para Portugues: o parametro para isso é o locale: ex: ```?locale=pt```ou ```?locale=en```

